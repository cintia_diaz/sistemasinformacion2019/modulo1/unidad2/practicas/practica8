﻿USE reservas;

/*
  CONSULTA 1:
  Numero de habitaciones por cada hotel (solo indicar el número del hotel)
*/
  SELECT NUM_HOTEL, COUNT(*) numHabitaciones FROM tipo_habitacion GROUP BY NUM_HOTEL;

  CREATE OR REPLACE VIEW vista1 AS
    SELECT 
      NUM_HOTEL, COUNT(*) numHabitaciones 
    FROM tipo_habitacion
    GROUP BY NUM_HOTEL;

  SELECT * FROM vista1 v;

   
 /*
  CONSULTA 2:
  Numero de habitaciones por cada hotel (indicar el nombre y la dirección del hotel)
*/
 
  CREATE OR REPLACE VIEW vista2 AS
    SELECT 
      h.NOMBRE,h.DOMICILIO,v.numHabitaciones 
    FROM vista1 v 
    JOIN hotel h 
    USING (NUM_HOTEL);

  SELECT * FROM vista2 v;

  
/*
  CONSULTA 3:
  Indicar el nombre del hotel con el mayor numero de habitaciones
*/
  

  CREATE OR REPLACE VIEW vista3 AS
    SELECT 
      nombre 
    FROM vista2 v 
    WHERE numHabitaciones=(SELECT MAX(numHabitaciones) maximo FROM vista2 v);

  SELECT * FROM vista3 v;

  
/*
  CONSULTA 4:
  Indicar el nombre del hotel (o hoteles) con el segundo mayor numero de habitaciones (es 12 y no 15)
*/
 
  
  CREATE OR REPLACE VIEW vista4 AS
    SELECT 
      nombre 
    FROM vista2 v 
    WHERE numhabitaciones=(
      SELECT numHabitaciones 
      FROM vista2 v
      ORDER BY numHabitaciones DESC LIMIT 2,1
    );
  
  SELECT * FROM vista4;

/*
  CONSULTA 5:
*/
-- Indicar el nombre de los hoteles que todavia no tenemos habitaciones introducidas

  -- c1: hoteles con habitaciones introducidas
  CREATE OR REPLACE VIEW vista5a AS
    SELECT 
      DISTINCT th.NUM_HOTEL 
    FROM tipo_habitacion th;

  -- final: hoteles menos los hoteles en los que tenemos habitaciones introducidas
  CREATE OR REPLACE VIEW vista5 AS
    SELECT 
      h.NOMBRE 
    FROM hotel h LEFT JOIN vista5a v USING (NUM_HOTEL)
    WHERE v.NUM_HOTEL IS NULL;

  SELECT * FROM vista5;

/*
  CONSULTA 6:
  Indicar el nombre del hotel del cual no se han reservado nunca habitaciones (teniendo
  en cuenta solamente los hoteles de los cuales hemos introducido sus habitaciones)
*/

 
 

/*
  CONSULTA 7:
  Indicar el mes que mas reservas han comenzado
*/
  -- c1: Cuento número de reservas por mes
  CREATE OR REPLACE VIEW vista7a AS
    SELECT 
      MONTH(r.FECHA_INI) mes, COUNT(*) numReservas 
    FROM reserva r 
    GROUP BY MONTH(r.FECHA_INI);

  -- c2: Calculo el máximo del número de reservas
  CREATE OR REPLACE VIEW vista7b AS
    SELECT 
      MAX(numReservas) maximo 
    FROM vista7a;;
  
  -- final: Calculo el mes con mayor número de reservas que comienzan
  CREATE OR REPLACE VIEW vista7 AS
   SELECT 
    mes 
   FROM vista7a
   JOIN vista7b
   ON numReservas=maximo;

  SELECT * FROM vista7;
  

/*
  CONSULTA 8:
  Listar el num_hotel y los distintos meses en que se han comenzado reservas
*/

  CREATE OR REPLACE VIEW vista8 AS
    SELECT 
      DISTINCT h.NUM_HOTEL, MONTH(r1.FECHA_INI) mes
    FROM hotel h 
    JOIN tipo_habitacion th 
    USING (NUM_HOTEL) 
    JOIN reserva r1
    USING (NUM_TIPOHAB)
    ORDER BY 1, 2;

  SELECT * FROM vista8 v;

/*
  CONSULTA 9:
  Listar los nombres de los hoteles que han comenzado reservas en todos los meses 
  que han comenzado reservas el hotel numero 4
*/
  
/*
  CONSULTA 10:
  Listar los días totales que se han reservado cada una de las habitaciones. Indicar en el listado
  el nombre del hotel al que pertenece
*/

  -- vista10a: días reservados según número de tipo de habitacion
  CREATE OR REPLACE VIEW vista10a AS
    SELECT 
      NUM_TIPOHAB, ABS(DATEDIFF(FECHA_FIN, FECHA_INI)) dias 
    FROM reserva;

  -- vista10b: número total de días reservados por número de tipo de habitacion
  CREATE OR REPLACE VIEW vista10b AS
    SELECT 
      v.NUM_TIPOHAB, SUM(v.dias) diasTotales
    FROM vista10a v
    GROUP BY v.NUM_TIPOHAB;

  -- final: dias totales que se han reservado las habitaciones por número de tipo de habitación y nombre de hotel

    CREATE OR REPLACE VIEW vista10 AS
      SELECT NUM_TIPOHAB habitacion, h.NOMBRE hotel, diasTotales 
      FROM vista10b 
      JOIN tipo_habitacion th 
      USING (NUM_TIPOHAB) 
      JOIN hotel h USING (NUM_HOTEL);

    SELECT * FROM vista10 v; 


/*
  CONSULTA 11:
  Indicar el nombre de los 2 hoteles que mayor número de habitaciones se han reservado durante
  el primer trimestre de cualquier año
*/

-- c1: Tipo de habitaciones reservadas en el primer trimestre del año

  CREATE OR REPLACE VIEW vista11a AS
    SELECT r.NUM_TIPOHAB 
    FROM reserva r
    WHERE QUARTER(r.FECHA_INI)=1;

-- c2: Numero de habitaciones reservadas en el primer trimestre del año por hotel
  CREATE OR REPLACE VIEW vista11b AS
    SELECT 
      th.NUM_HOTEL, COUNT(*) nHabitaciones
    FROM vista11a v 
    JOIN tipo_habitacion th 
    USING(NUM_TIPOHAB)
    GROUP BY th.NUM_HOTEL;

  
-- final: Nombre de los 2 hoteles con mayor número de habitaciones reservadas en el primer trimestre del año

  CREATE OR REPLACE VIEW vista11 AS
    SELECT 
      h.NOMBRE 
    FROM vista11b 
    JOIN hotel h 
    USING (NUM_HOTEL) 
    ORDER BY nHabitaciones DESC 
    LIMIT 0,2;
  
  SELECT * FROM vista11;
