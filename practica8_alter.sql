﻿USE reservas;

ALTER TABLE tipo_habitacion
  ADD CONSTRAINT fkhotel_tipo_habitacion  FOREIGN KEY (NUM_HOTEL) 
  REFERENCES hotel (NUM_HOTEL);

ALTER TABLE reserva
  ADD CONSTRAINT fkreserva_cliente FOREIGN KEY (NUM_CLIENTE)
  REFERENCES cliente (NUM_CLIENTE),
  ADD CONSTRAINT fkreserva_tipo_habitacion FOREIGN KEY (NUM_TIPOHAB)
  REFERENCES tipo_habitacion (NUM_TIPOHAB);